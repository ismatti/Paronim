package com.ismatti.paronim.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ismatti.paronim.R
import com.ismatti.paronim.activity.NewsDetailActivity
import com.ismatti.paronim.model.News

class NewsListAdapter(
    context: Context?,
    countries: List<News>
) :
    RecyclerView.Adapter<NewsListAdapter.ViewHolder>() {
    var inflater: LayoutInflater
    var newsList: List<News>
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View = inflater.inflate(R.layout.news_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val news: News = newsList[position]
        holder.newsTitle.setText(news.title)

        var field = R.drawable::class.java.getDeclaredField(news.image)
        holder.imageView.setImageResource(field.getInt(field))

    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        val newsTitle: TextView
        val imageView: ImageView

        override fun onClick(v: View) {
//            Toast.makeText(v.getContext(), "position = " + getLayoutPosition(), Toast.LENGTH_SHORT).show();
            val item = layoutPosition + 1
            val intent = Intent(v.context, NewsDetailActivity::class.java)
            intent.putExtra("numNews", item)
            v.context.startActivity(intent)
        }

        init {
            view.setOnClickListener(this)
            newsTitle = view.findViewById(R.id.newsTitle)
            imageView= view.findViewById(R.id.imageView)

        }
    }

    init {
        newsList = countries
        inflater = LayoutInflater.from(context)
    }
}