package com.ismatti.paronim.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ismatti.paronim.R
import com.ismatti.paronim.model.User

class ResultListAdapter(
    context: Context?,
    countries: List<User>
) :
    RecyclerView.Adapter<ResultListAdapter.ViewHolder>() {
    var inflater: LayoutInflater
    var usersList: List<User>
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View = inflater.inflate(R.layout.rating_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val user: User = usersList[position]
        holder.num.setText((position+1).toString())
        holder.name.setText(user.name)
        holder.lives.setText(user.lives.toString())
        holder.time.setText("${user.time/1000/60} мин ${user.time/1000%60} секунд ")


    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        val num: TextView
        val name: TextView
        val lives: TextView
        val time: TextView
        override fun onClick(v: View) {

        }

        init {
            view.setOnClickListener(this)
            num = view.findViewById(R.id.tvNum)
            name = view.findViewById(R.id.tvName)
            lives = view.findViewById(R.id.tvLives)
            time = view.findViewById(R.id.tvTime)
        }
    }

    init {
        usersList = countries
        inflater = LayoutInflater.from(context)
    }
}