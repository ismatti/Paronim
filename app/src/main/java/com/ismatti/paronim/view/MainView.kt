package com.ismatti.paronim.view

import com.ismatti.paronim.model.News
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndStrategy::class)
interface MainView : MvpView {
    fun showAllNews(news:ArrayList<News>)
}