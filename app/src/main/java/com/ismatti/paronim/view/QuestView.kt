package com.ismatti.paronim.view

import com.ismatti.paronim.fragment.FragmentResult
import com.ismatti.paronim.model.Quest
import com.ismatti.paronim.model.User
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndStrategy::class)
interface QuestView : MvpView {
    fun showQuest(quest: Quest)
    fun showRating(users:ArrayList<User>)
    fun showResult(frag:FragmentResult)
    fun showError()
    fun showNext()
    fun startProgress()
    fun stopProgress()

}