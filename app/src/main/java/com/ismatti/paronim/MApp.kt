package com.ismatti.paronim

import android.app.Application
import com.ismatti.paronim.di.component.AppComp
import com.ismatti.paronim.di.component.DaggerAppComp
import com.ismatti.paronim.di.module.DbModule

class MApp : Application() {
    companion object{
         var appComp:AppComp?=null
    }
    override fun onCreate(){
        super.onCreate()
        appComp = DaggerAppComp.builder().dbModule(DbModule(applicationContext)).build()
    }
}