package com.ismatti.paronim.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "quest")
data class Quest(
    @ColumnInfo(name="id") @PrimaryKey val id:Int,
    @ColumnInfo(name="text") var text:String,
    @ColumnInfo(name="variant1") var variant1:String,
    @ColumnInfo(name="variant2") var variant2:String,
    @ColumnInfo(name="answer") var answer:String,
    @ColumnInfo(name="explanation") var explanation:String
) {
}