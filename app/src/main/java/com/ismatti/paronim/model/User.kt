package com.ismatti.paronim.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class User (

    @ColumnInfo(name="name") var name:String,
    var lives:Int,
    var time:Long
) : Serializable{
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    constructor() : this("",0,0)

}