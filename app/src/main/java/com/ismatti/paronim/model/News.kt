package com.ismatti.paronim.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "news")
data class News(@ColumnInfo(name="id") @PrimaryKey val id:Int,
                @ColumnInfo(name="title") var title:String,
                @ColumnInfo(name="textNews") var textNews:String,
                @ColumnInfo(name="additionText") var additionText:String?,
                @ColumnInfo(name="image") var image:String){
}