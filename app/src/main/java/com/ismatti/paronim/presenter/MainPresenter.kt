package com.ismatti.paronim.presenter

import com.ismatti.paronim.MApp
import com.ismatti.paronim.db.dao.NewsDao
import com.ismatti.paronim.model.News
import com.ismatti.paronim.view.MainView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moxy.MvpPresenter
import javax.inject.Inject

class MainPresenter : MvpPresenter<MainView> {
    @Inject
    lateinit var newsDao: NewsDao

    constructor(){
        MApp.appComp?.inject(this)
    }

    fun getAllNews(){
        GlobalScope.launch(Dispatchers.Main) {

           viewState.showAllNews(newsDao.getAllNews() as ArrayList<News>)
        }
    }

}