package com.ismatti.paronim.presenter

import com.ismatti.paronim.MApp
import com.ismatti.paronim.db.dao.NewsDao
import com.ismatti.paronim.view.NewsView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moxy.MvpPresenter
import javax.inject.Inject

class NewsPresenter : MvpPresenter<NewsView> {
    @Inject
    lateinit var newsDao: NewsDao

    constructor(){
        MApp.appComp?.inject(this)
    }

    fun getNews(id:Int){
        GlobalScope.launch(Dispatchers.Main) {
           viewState.showNews(newsDao.getNewsByID(id))
        }
    }

}