package com.ismatti.paronim.presenter

import android.graphics.Color
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.ismatti.paronim.MApp
import com.ismatti.paronim.db.dao.QuestDao
import com.ismatti.paronim.db.dao.UserDao
import com.ismatti.paronim.fragment.FragmentResult
import com.ismatti.paronim.model.Quest
import com.ismatti.paronim.model.User
import com.ismatti.paronim.view.QuestView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import moxy.MvpPresenter
import javax.inject.Inject

class QuestPresenter : MvpPresenter<QuestView> {
    @Inject
    lateinit var questDao: QuestDao
    @Inject
    lateinit var userDao: UserDao
    lateinit var quests: ArrayList<Quest>
    lateinit var curQuest: Quest
    lateinit var user: User
    var startTime:Long=0
    var endTime:Long=0
    var frag: FragmentResult = FragmentResult("","","",0)
    var restart:Boolean=true
    var loaded:Boolean = false
    var lives:Int=5


    constructor(){
        MApp.appComp?.inject(this)
        load()
    }
    fun loadQuest(){
        loaded = true
        
        viewState.showQuest(curQuest)
        viewState.showNext()

    }

    fun load(){
        if(startTime==0L) {
            startTime = System.currentTimeMillis()
        }


        GlobalScope.launch {
            user = userDao.getAllUsers().get(0)
            quests = questDao.getAllQuests() as ArrayList<Quest>
            var curNum:Int = (1 until quests.size).random()
            curQuest = quests?.removeAt(curNum)
            //viewState.showQuest(curQuest)


        }
    }
    fun nextQuest(){

        if(quests.size<=1){
            //GlobalScope.launch(Dispatchers.Main) {
                endTime = System.currentTimeMillis()
                user.lives=lives
                user.time=endTime-startTime
                initRating()

        }else {
            var curNum: Int = (1 until quests.size).random()
            curQuest = quests?.removeAt(curNum)
        }
    }

    fun initRating(){
        var users:ArrayList<User> = ArrayList()

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference().child("users")
        myRef.push().setValue(user)

        myRef.addChildEventListener(object :ChildEventListener {
            var count=0
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {

            }


            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                count++;
                users.add(p0.getValue(User::class.java)!!)
                var ch = p0.getChildrenCount()
                println("$count  $ch")
                if(count >= p0.getChildrenCount()){

                }


            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }


        })


        GlobalScope.launch(Dispatchers.Main) {
            viewState.startProgress()
            delay(3000)
            viewState.showRating(users)
        }

    }

    fun check(think:String){
        if(!restart) {
                if (think.equals(curQuest.answer)) {
                    frag = FragmentResult("Верно", curQuest.answer, curQuest.explanation, Color.parseColor("#F0FFE6"))
                } else {
                    frag = FragmentResult("Неверно", curQuest.answer, curQuest.explanation, Color.parseColor("#FFEEEE"))
                    lives--
                }
        }

        viewState.showResult(frag)
        if(lives<=0){
            viewState.showError()
        }
    }

}