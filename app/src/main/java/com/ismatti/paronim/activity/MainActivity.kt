package com.ismatti.paronim.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.ismatti.paronim.MApp
import com.ismatti.paronim.R
import com.ismatti.paronim.adapter.NewsListAdapter
import com.ismatti.paronim.model.News
import com.ismatti.paronim.presenter.MainPresenter
import com.ismatti.paronim.view.MainView
import kotlinx.android.synthetic.main.activity_main.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter


class MainActivity : MvpAppCompatActivity(), MainView, View.OnClickListener{
    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    lateinit var but: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        butStart.setOnClickListener(this)
        MApp.appComp?.inject(this)
        mainPresenter.getAllNews()

    }
    override fun showAllNews(news:ArrayList<News>){
        val adapter = NewsListAdapter(this,news)
        recNews.setAdapter(adapter)
    }
    override fun onClick(v: View?) {
        when(v?.getId()){
            R.id.butStart->{
                startActivity(Intent(this,QuestActivity::class.java))
            }
        }
    }


}