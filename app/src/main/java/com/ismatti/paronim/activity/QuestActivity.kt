package com.ismatti.paronim.activity


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import com.ismatti.paronim.R
import com.ismatti.paronim.databinding.ActivityQuestBinding
import com.ismatti.paronim.fragment.FragmentResult
import com.ismatti.paronim.fragment.RatingFragment
import com.ismatti.paronim.model.Quest
import com.ismatti.paronim.model.User
import com.ismatti.paronim.presenter.QuestPresenter
import com.ismatti.paronim.view.QuestView
import kotlinx.android.synthetic.main.activity_quest.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class QuestActivity() : MvpAppCompatActivity(), QuestView{
    @InjectPresenter
    lateinit var presenter: QuestPresenter;
    lateinit var rightAns:String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quest)
        if(!presenter.loaded){
            presenter.loadQuest()
        }
        presenter.restart=true
    }


    @RequiresApi(Build.VERSION_CODES.N)
    override fun showQuest(quest: Quest) {
        val binding: ActivityQuestBinding = DataBindingUtil.setContentView(this,R.layout.activity_quest)
        binding.setQuest(quest)
        binding.setPresenter(presenter)
//        setSupportActionBar(toolbarQuest)
//        supportActionBar.apply {
//            toolbarQuest.title = "Паронины"
//            toolbarQuest.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
//            toolbarQuest.setNavigationOnClickListener({ view: View? -> finish() })
//        }
        println("show")
    }

    override fun showRating(users: ArrayList<User>) {
        GlobalScope.launch(Dispatchers.Main) {
            delay(100)
        stopProgress()
            restartBut.setVisibility(View.VISIBLE)
             var param = LinearLayout.LayoutParams(restartBut.getLayoutParams())
            param.gravity=Gravity.RIGHT
            restartBut.setLayoutParams(param)
            val ratingList = users.sortedWith(object:Comparator<User>{
                override fun compare(o1: User, o2: User): Int {
                    when{
                        o1.lives<o2.lives -> return 1
                        o1.lives==o2.lives ->
                            when{
                                o1.time < o2.time -> return -1
                                o1.time > o2.time -> return 1
                                else -> return 0
                            }
                        else -> return -1
                    }
                }

            })
        for(us in ratingList){

            if(us.equals(presenter.user)){

                testTask.setText(" ${us.name} ваше место ${ratingList.indexOf(us)+1} ")
                break
            }
        }
        var ft = supportFragmentManager.beginTransaction()
        var ratFrag = RatingFragment()
        ratFrag.ratingList = ratingList
        ft.replace(R.id.frameRating, ratFrag)
        ft.commit()
        }
    }

    fun check(v:View){
        presenter.restart=false
        presenter.check((v as TextView).text.toString())
        if(presenter.lives<=0) return
        presenter.nextQuest()
        presenter.loadQuest()

    }
    override fun showResult(frag:FragmentResult){
        var ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameLay,frag)
        ft.commit()


    }

    override fun showError() {
        GlobalScope.launch(Dispatchers.Main) {
            delay(100)
            tvVar1.setVisibility(View.GONE)
            tvVar2.setVisibility(View.GONE)
            testTask.setText("Вы допустили пять ошибок")
            restartBut.setVisibility(View.VISIBLE)

                }

    }

    override fun showNext() {
//        GlobalScope.launch(Dispatchers.Main) {
//            delay(700)
//            println("setrat ${presenter.lives.toFloat()}")
//
//            ratingBar.setRating(presenter.lives.toFloat())
//
//        }

    }

    override fun startProgress() {
        println("progress")
        testTask.setText("Загрузка")
        tvVar1.setVisibility(View.GONE)
        tvVar2.setVisibility(View.GONE)

        progressBar.setVisibility(View.VISIBLE)
    }

    override fun stopProgress() {
        testTask.setText("Рейтинг")
        progressBar.setVisibility(View.GONE)
    }
    fun restart(v:View){
        startActivity(Intent(this, QuestActivity::class.java))
    }
    fun finish(v:View){
        finish()
    }


}