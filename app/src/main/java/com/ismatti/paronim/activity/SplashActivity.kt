package com.ismatti.paronim.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.ismatti.paronim.MApp
import com.ismatti.paronim.R
import com.ismatti.paronim.db.dao.QuestDao
import com.ismatti.paronim.db.dao.UserDao
import com.ismatti.paronim.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {
    @Inject
    lateinit var userDao: UserDao
    @Inject
    lateinit var questDao: QuestDao
    lateinit var users:List<User>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        MApp.appComp?.inject(this)
        Handler().postDelayed({
            GlobalScope.launch {

                users = userDao.getAllUsers()

                if(users.isEmpty()){

                    val mIntent = Intent(this@SplashActivity, RegisterActivity::class.java)
                    startActivity(mIntent)
                    finish()

                }else{


                    val mIntent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(mIntent)
                    finish()

                }
            }
        },2000)


    }
}