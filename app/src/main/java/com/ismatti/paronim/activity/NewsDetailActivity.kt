package com.ismatti.paronim.activity

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.ismatti.paronim.R
import com.ismatti.paronim.databinding.ActivityNewsDetailBinding
import com.ismatti.paronim.model.News
import com.ismatti.paronim.presenter.NewsPresenter
import com.ismatti.paronim.view.NewsView
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class NewsDetailActivity : MvpAppCompatActivity(), NewsView {
    @InjectPresenter
    lateinit var newsPresenter: NewsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        newsPresenter.getNews(getIntent().getIntExtra("numNews",1))


    }

    override fun showNews(news: News) {
        val binding: ActivityNewsDetailBinding = DataBindingUtil.setContentView(this,R.layout.activity_news_detail)
        binding.setNews(news)
//        setSupportActionBar(toolbarNews)
//        supportActionBar.apply {
//
//            toolbarNews.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
//            toolbarNews.setNavigationOnClickListener({ view: View? -> finish() })
//        }
    }
    fun finish (v: View){
        finish()
    }
}