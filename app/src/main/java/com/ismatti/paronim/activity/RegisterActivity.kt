package com.ismatti.paronim.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ismatti.paronim.MApp
import com.ismatti.paronim.R
import com.ismatti.paronim.db.dao.UserDao
import com.ismatti.paronim.fragment.PolicyFragment
import com.ismatti.paronim.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class RegisterActivity : AppCompatActivity() {
    @Inject
    lateinit var userDao: UserDao
    lateinit var nameText: EditText
    lateinit var but: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        MApp.appComp?.inject(this)
        nameText = findViewById(R.id.regName)
    }
    fun reg(v: View){
        var name=nameText.text.toString()
        if(name.length<1){
            Toast.makeText(this,"Введите имя",Toast.LENGTH_SHORT).show()
        }else{

            GlobalScope.launch { userDao.addUser(User(name,5,0)) }
            startActivity(Intent(this, MainActivity::class.java))
        }

    }
    fun showPolicy(v:View){
        var ft = supportFragmentManager.beginTransaction()
        var polFrag = PolicyFragment()
        ft.replace(R.id.policyFrame, polFrag)
        ft.commit()
    }
}