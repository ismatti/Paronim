package com.ismatti.paronim.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.ismatti.paronim.R
import kotlinx.android.synthetic.main.fragment_result.view.*


class FragmentResult (
    var result:String="0",
    var rightAnswer:String="0",
    var explanation:String="0",
    var color:Int
) : Fragment() {
    constructor() : this("0","0","0",0)


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_result,null)
        rootView.fragResTv.text = result
        rootView.fragAnsTv.text = rightAnswer
        rootView.fragExpTv.setText(Html.fromHtml(explanation, Html.FROM_HTML_MODE_COMPACT))
        rootView.setBackgroundColor(color)

        return rootView
    }
}