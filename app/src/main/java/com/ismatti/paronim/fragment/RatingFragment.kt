package com.ismatti.paronim.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ismatti.paronim.R
import com.ismatti.paronim.adapter.ResultListAdapter
import com.ismatti.paronim.model.User
import kotlinx.android.synthetic.main.activity_result.view.*

class RatingFragment (): Fragment() {
    var ratingList:List<User> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.activity_result,null)
//        val rl = ratingList.sortedWith(object:Comparator<User>{
//            override fun compare(o1: User, o2: User): Int {
//                when{
//                    o1.lives<o2.lives -> return 1
//                    o1.lives==o2.lives ->
//                        when{
//                            o1.time < o2.time -> return -1
//                            o1.time > o2.time -> return 1
//                            else -> return 0
//                        }
//                    else -> return -1
//                }
//            }
//
//        })
       val adapter = ResultListAdapter(context, ratingList)
        rootView.recView.setAdapter(adapter)

        return rootView


    }
}