package com.ismatti.paronim.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.ismatti.paronim.db.dao.NewsDao
import com.ismatti.paronim.db.dao.QuestDao
import com.ismatti.paronim.db.dao.UserDao
import com.ismatti.paronim.model.News
import com.ismatti.paronim.model.Quest
import com.ismatti.paronim.model.User

@Database(entities = arrayOf(Quest::class, User::class, News::class), version = 2, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun getQuestDao():QuestDao
    abstract fun getUserDao(): UserDao
    abstract fun getNewsDao(): NewsDao
    companion object{
        val MIGRATION_1_2 = object : Migration(1, 2){
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS 'news' ( 'id' INTEGER, 'title' TEXT, 'textNews' TEXT, 'additionText' TEXT, 'image' TEXT, PRIMARY KEY('id') )")
            }
        }
    }

}