package com.ismatti.paronim.db.dao

import androidx.room.*
import com.ismatti.paronim.model.News

@Dao
interface NewsDao {
    @Query("SELECT*FROM news")
    suspend fun getAllNews():List<News>
    @Query("SELECT*FROM news WHERE id=:id")
    suspend fun getNewsByID(id:Int): News
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addNews(news: News)
    @Update
    suspend fun updateNews(news: News)
    @Delete
    suspend fun deleteNews(news: News)
}