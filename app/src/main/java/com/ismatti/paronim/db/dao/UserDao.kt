package com.ismatti.paronim.db.dao

import androidx.room.*
import com.ismatti.paronim.model.User

@Dao
interface UserDao {
    @Query("SELECT*FROM user")
    suspend fun getAllUsers():List<User>
    @Query("SELECT*FROM user WHERE name=:name")
    suspend fun getUserByID(name:Int): User
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUser(user: User)
    @Update
    suspend fun updateUser(user: User)
    @Delete
    suspend fun deleteUser(user: User)
}