package com.ismatti.paronim.db.dao

import androidx.room.*
import com.ismatti.paronim.model.Quest

@Dao
interface QuestDao {
    @Query("SELECT*FROM quest")
    suspend fun getAllQuests():List<Quest>
    @Query("SELECT*FROM quest WHERE id=:id")
    suspend fun getQuestByID(id:Int):Quest
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addQuest(quest:Quest)
    @Update
    suspend fun updateQuest(quest:Quest)
    @Delete
    suspend fun deleteQuest(quest:Quest)
}