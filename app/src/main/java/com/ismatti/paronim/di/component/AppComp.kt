package com.ismatti.paronim.di.component

import com.ismatti.paronim.activity.MainActivity
import com.ismatti.paronim.activity.RegisterActivity
import com.ismatti.paronim.activity.SplashActivity
import com.ismatti.paronim.db.dao.NewsDao
import com.ismatti.paronim.db.dao.QuestDao
import com.ismatti.paronim.db.dao.UserDao
import com.ismatti.paronim.di.module.DbModule
import com.ismatti.paronim.presenter.MainPresenter
import com.ismatti.paronim.presenter.NewsPresenter
import com.ismatti.paronim.presenter.QuestPresenter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DbModule::class])
interface AppComp {
    fun getQuestDao(): QuestDao
    fun getUserDao(): UserDao
    fun getNewsDao(): NewsDao
    fun inject(mainActivity: MainActivity)
    fun inject(splashActivity: SplashActivity)
    fun inject(registerActivity: RegisterActivity)
    fun inject(questPresenter: QuestPresenter)
    fun inject(newsPresenter: NewsPresenter)
    fun inject(mainPresenter: MainPresenter)
}