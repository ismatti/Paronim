package com.ismatti.paronim.di.module

import android.content.Context
import androidx.room.Room
import com.ismatti.paronim.db.AppDatabase
import com.ismatti.paronim.db.dao.NewsDao
import com.ismatti.paronim.db.dao.QuestDao
import com.ismatti.paronim.db.dao.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule(private val context : Context) {
    @Singleton
    @Provides
    fun provideContext():Context{return  context}

    @Singleton
    @Provides
    fun provideDb(context: Context):AppDatabase{
       return Room.databaseBuilder(context,AppDatabase::class.java,"ParonimDB").createFromAsset("ParonimDB.db").addMigrations(AppDatabase.MIGRATION_1_2).build()
    }

    @Singleton
    @Provides
    fun provideQuestDao(database: AppDatabase):QuestDao{
        return database.getQuestDao()
    }

    @Singleton
    @Provides
    fun provideUserDao(database: AppDatabase): UserDao {
        return database.getUserDao()
    }

    @Singleton
    @Provides
    fun provideNewsDao(database: AppDatabase): NewsDao {
        return database.getNewsDao()
    }


}